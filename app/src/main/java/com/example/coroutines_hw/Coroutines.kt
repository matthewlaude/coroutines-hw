package com.example.coroutines_hw

import java.lang.Exception

class Coroutines (
    private val mockApi: MockApi,
    private val viewModelScope: CoroutineScope
) {

    fun performSingleNetworkRequest() {
        viewModelScope.launch {
            val users = try {
                mockApi.getUsers()
            } catch (exception: Exception) {
                emptyList<>()
            }
        }
    }

    fun performTwoSequentialRequest() {
        viewModelScope.launch {
            val users = try {
                mockApi.getUsers()
                mockApi.getCategories()
            } catch (ex: Exception) {
                emptyList<>()
            }
        }
    }

    fun performTwoConcurrentRequests() {
        viewModelScope.async {
            mockApi.getUsers()
            mockApi.getCategories()
        }
    }

    fun performDynamicAmountOfRequests() {
        viewModelScope.launch {
            mockApi.getCategories().map { mockApi.getCategoryList(it) }
        }
    }

    class CoroutineScope {

    }
}


